// Определить интерфейс ITicket

export interface ITicket {
    description: string;
    name: string;
    price: string;
    tourOperator: string;
    hotel: string;
}

// Определить интерфейс IVipTicket который расширяет  ITicket

export interface IVipTicket extends ITicket {
    vipNumber: number;
    vipStatus: string;
}

// Определить тип TicketType который будет объединять 2 интерфейса IVipTicket и ITicket

export type TicketType = ITicket | IVipTicket;

